# Smart-test



**# En la carpeta /src/main/request   se pueden econtrar postman collections y el diagrama.**






#### 1.Api encargada de autorizar el acceso a los recursos mediante un token.
### Endpoint
> http://localhost:9595/authenticate POST

> Request body :
 
``` json
{
	
	"username":"smartUser",
	"password":"password"
	
}
```

> Response :
 
``` json
{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydFVzZXIiLCJleHAiOjE1ODM3NjM5NjEsImlhdCI6MTU4Mzc0NTk2MX0.7lopSQID56pgExT8pIvfNmMq0tvIVbDGST5Ke1CnAGVn11yFkpFwgfL_sKGuKUAUdsF5gQnPNSMa5ATzQKcvnw"
}
```




#### 2.Api encargada de regresar todas las organizaciones 
### Endpoint


> http://localhost:9595/api/public/v1/organization GET


Es necesario mandar el header Authorization  de la siguiente manera:

bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydFVzZXIiLCJleHAiOjE1ODM3NjM5NjEsImlhdCI6MTU4Mzc0NTk2MX0.7lopSQID56pgExT8pIvfNmMq0tvIVbDGST5Ke1CnAGVn11yFkpFwgfL_sKGuKUAUdsF5gQnPNSMa5ATzQKcvnw

> Request body :

``` json
```

> Response :
 
``` json
{
    "orgnizationList": [

         {
            "id": "5e66060c59012a73127f8889",
            "name": "smartqqqqqqqqqqqqqqqqqqqq",
            "address": "address",
            "phone": "1234567890",
            "externalId": "smar7890NE6",
            "encryptionCode": "%*),$+'"
        },
        {
            "id": "5e66125459012a73127f888f",
            "name": "smar12132141432132",
            "address": "address",
            "phone": "1234567890",
            "externalId": "smar7890NE7",
            "encryptionCode": "%+&+-')"
        }
    ]
}
```


#### 3.Api encargada de agregar  una nueva organización
### Endpoint
> http://localhost:9595/api/public/v1/organization POST

Es necesario mandar el header Authorization  de la siguiente manera:

bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydFVzZXIiLCJleHAiOjE1ODM3NjM5NjEsImlhdCI6MTU4Mzc0NTk2MX0.7lopSQID56pgExT8pIvfNmMq0tvIVbDGST5Ke1CnAGVn11yFkpFwgfL_sKGuKUAUdsF5gQnPNSMa5ATzQKcvnw


> Request body :

``` json
{
	
	"name":"name",
	"address":"address",
	"phone":"1234567890"
	
}

```



> Response :
 
``` json
{
    "id": "5e66169359012a7f370360ab",
    "name": "name",
    "address": "address",
    "phone": "1234567890",
    "externalId": "name7890NE8",
    "encryptionCode": "*&-)),&"
}
```



#### 4.Api encargada de buscar y regresar un organización por id.
### Endpoint
> http://localhost:9595/api/public/v1/organization/{id} GET
http://localhost:9595/api/public/v1/organization/5e66169359012a7f370360ab

Es necesario mandar los siguientes Headers

**Authorization :**

bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydFVzZXIiLCJleHAiOjE1ODM3NjM5NjEsImlhdCI6MTU4Mzc0NTk2MX0.7lopSQID56pgExT8pIvfNmMq0tvIVbDGST5Ke1CnAGVn11yFkpFwgfL_sKGuKUAUdsF5gQnPNSMa5ATzQKcvnw

**encryptionCode :**

*&-)),&


> Request body :
 
``` json
```


> Response :
 
``` json
{
    "id": "5e66169359012a7f370360ab",
    "name": "name",
    "address": "address",
    "phone": "1234567890",
    "externalId": "name7890NE8",
    "encryptionCode": "*&-)),&"
}
```



#### 5.Api encargada de actualizar una organización por id. 
### Endpoint
> http://localhost:9595/api/public/v1/organization/{id} PUT
http://localhost:9595/api/public/v1/organization/5e66169359012a7f370360ab 

Es necesario mandar los siguientes Headers

**Authorization :**

bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydFVzZXIiLCJleHAiOjE1ODM3NjM5NjEsImlhdCI6MTU4Mzc0NTk2MX0.7lopSQID56pgExT8pIvfNmMq0tvIVbDGST5Ke1CnAGVn11yFkpFwgfL_sKGuKUAUdsF5gQnPNSMa5ATzQKcvnw

**encryptionCode :**

*&-)),&

> Request body :

``` json

{
    "id": "5e66169359012a7f370360ab",
    "name": "name99",
    "address": "address",
    "phone": "1234567890",
    "externalId": "name7890NE8",
    "encryptionCode": ",#-#*-,"
}

```


> Response :

``` json
{
    "id": "5e66169359012a7f370360ab",
    "name": "name99",
    "address": "address",
    "phone": "1234567890",
    "externalId": "name7890NE9",
    "encryptionCode": ",#-#*-,"
}
```


#### 6.Api encargada de eliminar una organización por id. 
### Endpoint
> http://localhost:9595/api/public/v1/organization/{id} DELETE http://localhost:9595/api/public/v1/organization/5e65e50a59012a4b4f7cd9d0

**Authorization :**

bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbWFydFVzZXIiLCJleHAiOjE1ODM3NjM5NjEsImlhdCI6MTU4Mzc0NTk2MX0.7lopSQID56pgExT8pIvfNmMq0tvIVbDGST5Ke1CnAGVn11yFkpFwgfL_sKGuKUAUdsF5gQnPNSMa5ATzQKcvnw

**encryptionCode :**

*&-)),&


> Request body :

``` json
```

> Response :

``` json
```

## Built With
* Maven
* SpringBoot
* Spring Tools Suite
* Lombok

### Prerequisites
Se necesita tener instalado:

* Maven versión 3.6.0
* Java version 8



**Variables de Entorno **


* jwt.secret=smartsecret
* jwt.get.token.uri=/authenticate
* constants.api.uri.basePath=/api/public/v1/organization
* spring.data.mongodb.uri = mongodb+srv://mongo:qgmeDA4YL90AmEaB@edmund-cluster-xhlnr.mongodb.net/management_files?retryWrites=true&w=majority
* spring.data.mongodb.database=smart_test
* server.port=9595


## Deployment

    java -jar nombre_delarchivo_.jar
