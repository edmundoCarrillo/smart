package com.smart.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class OrganizationDTO {

	private String id;
	private String name;
	private String address;
	private String phone;
	private String externalId;
	private String encryptionCode;

}
