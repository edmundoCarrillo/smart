package com.smart.test.model;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoadResponse {

	private List<OrganizationDTO> orgnizationList;

}
