package com.smart.test.constant;

import lombok.Getter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@Getter
public class ApiValues {

	public static final String BASE_PATH = "${constants.api.uri.basePath}";
	public static final String RETRIEVE_ORGANIZATIONS_PATH = "${constants.api.uri.specificPaths.retrieve}";
	public static final String ENCRYPTION_CODE = "encryptionCode";

}
