package com.smart.test.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smart.test.constant.ApiValues;
import com.smart.test.domain.Organization;
import com.smart.test.model.LoadResponse;
import com.smart.test.model.OrganizationDTO;
import com.smart.test.service.OrganizationService;

@RestController
@CrossOrigin
@RequestMapping(value = ApiValues.BASE_PATH)
public class OrganizationController {

	@Autowired
	private OrganizationService organizationService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoadResponse> retrieveOrganizations() {
		return new ResponseEntity<>(organizationService.retrieveOrganizations(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<OrganizationDTO> saveOrganization(@RequestBody Organization organization) {
		return ResponseEntity.ok(organizationService.saveOrganization(organization));
	}

	@GetMapping("/{id}")
	public ResponseEntity<OrganizationDTO> findById(
			@RequestHeader(name = ApiValues.ENCRYPTION_CODE, required = true) String encryptionCode,
			@PathVariable String id) {

		OrganizationDTO organizationDTO = organizationService.findById(id);

		if (!organizationDTO.getEncryptionCode().equals(encryptionCode)) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(organizationDTO);
	}

	@PutMapping("/{id}")
	public ResponseEntity<OrganizationDTO> update(
			@RequestHeader(name = ApiValues.ENCRYPTION_CODE, required = true) String encryptionCode,
			@PathVariable String id, @RequestBody Organization organization) {

		OrganizationDTO organizationDTO = organizationService.findById(id);

		if (!organizationDTO.getEncryptionCode().equals(encryptionCode))
			return ResponseEntity.badRequest().build();

		if (organizationDTO.getName() == null)
			return ResponseEntity.notFound().build();

		organization.setId(id);

		return ResponseEntity.ok(organizationService.updateOrganization(organization));

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> delete(
			@RequestHeader(name = ApiValues.ENCRYPTION_CODE, required = true) String encryptionCode,
			@PathVariable String id) {

		OrganizationDTO organizationDTO = organizationService.findById(id);

		if (!organizationDTO.getEncryptionCode().equals(encryptionCode))
			return ResponseEntity.badRequest().build();

		if (organizationDTO.getName() == null)
			return ResponseEntity.notFound().build();

		organizationService.deleteById(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
