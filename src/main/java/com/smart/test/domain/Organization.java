package com.smart.test.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "organizations")
public class Organization {

	@Id
	private String id;
	private String name;
	private String address;
	private String phone;
	private String externalId;
	private String encryptionCode;

}
