package com.smart.test.util;

import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.smart.test.domain.Organization;
import com.smart.test.service.OrganizationService;

@Component
public class Util {

	@Autowired
	private OrganizationService organizationService;

	public String generateRandomSpecialCharacters(int length) {
		RandomStringGenerator pwdGenerator = new RandomStringGenerator.Builder().withinRange(33, 45).build();
		return pwdGenerator.generate(length);
	}

	public String createExternalId(Organization organization) {

		StringBuilder externalId = new StringBuilder();
		externalId.append(organization.getName().subSequence(0, 4));
		externalId.append(organization.getPhone().subSequence(organization.getPhone().length() - 4,
				organization.getPhone().length()));
		externalId.append("NE");
		externalId.append(organizationService.countOrganizations() + 1);

		return externalId.toString();

	}

}
