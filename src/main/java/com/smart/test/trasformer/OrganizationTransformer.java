package com.smart.test.trasformer;

import com.smart.test.domain.Organization;
import com.smart.test.model.OrganizationDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrganizationTransformer {

	@Autowired
	private ModelMapper modelMapper;

	public OrganizationDTO transformOrganizationModel(Organization organization) {
		return modelMapper.map(organization, OrganizationDTO.class);
	}

}
