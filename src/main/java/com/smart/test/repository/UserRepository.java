package com.smart.test.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smart.test.domain.User;

public interface UserRepository extends MongoRepository<User, String> {

	public User findByUserName(String userName);

}
