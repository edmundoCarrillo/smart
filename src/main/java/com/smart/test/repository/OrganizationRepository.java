package com.smart.test.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smart.test.domain.Organization;

public interface OrganizationRepository extends MongoRepository<Organization, String> {

}
