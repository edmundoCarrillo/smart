package com.smart.test.service;

import com.smart.test.domain.User;

public interface UserService {

	public User findByUserName(String userName);

}
