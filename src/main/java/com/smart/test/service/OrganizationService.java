package com.smart.test.service;

import com.smart.test.domain.Organization;
import com.smart.test.model.LoadResponse;
import com.smart.test.model.OrganizationDTO;

public interface OrganizationService {

	public OrganizationDTO saveOrganization(Organization organization);

	public OrganizationDTO updateOrganization(Organization organization);

	public LoadResponse retrieveOrganizations();

	public OrganizationDTO findById(String id);

	public Long countOrganizations();

	public void deleteById(String id);

}
