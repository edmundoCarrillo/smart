package com.smart.test.service.business;

import org.springframework.beans.factory.annotation.Autowired;

import com.smart.test.domain.User;
import com.smart.test.repository.UserRepository;
import com.smart.test.service.UserService;

public class UserServiceImpl implements UserService {

	private UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User findByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}

}
