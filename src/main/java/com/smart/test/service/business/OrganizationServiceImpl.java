package com.smart.test.service.business;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.test.domain.Organization;
import com.smart.test.model.LoadResponse;
import com.smart.test.model.OrganizationDTO;
import com.smart.test.repository.OrganizationRepository;
import com.smart.test.service.OrganizationService;
import com.smart.test.trasformer.OrganizationTransformer;
import com.smart.test.util.Util;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private OrganizationTransformer organizationTransformer;

	@Autowired
	private Util util;

	@Override
	public LoadResponse retrieveOrganizations() {

		List<Organization> organizationList = organizationRepository.findAll();
		List<OrganizationDTO> dtoList = organizationList.stream()
				.map(organizationTransformer::transformOrganizationModel).collect(Collectors.toList());

		return LoadResponse.builder().orgnizationList(dtoList).build();
	}

	@Override
	public OrganizationDTO saveOrganization(Organization organization) {
		organization.setEncryptionCode(util.generateRandomSpecialCharacters(7));
		organization.setExternalId(util.createExternalId(organization));
		return organizationTransformer.transformOrganizationModel(organizationRepository.save(organization));
	}

	@Override
	public OrganizationDTO updateOrganization(Organization organization) {
		organization.setExternalId(util.createExternalId(organization));
		return organizationTransformer.transformOrganizationModel(organizationRepository.save(organization));
	}

	@Override
	public OrganizationDTO findById(String id) {
		Optional<Organization> organization = organizationRepository.findById(id);
		return organizationTransformer.transformOrganizationModel(organization.get());
	}

	@Override
	public Long countOrganizations() {
		return organizationRepository.count();
	}

	@Override
	public void deleteById(String id) {
		organizationRepository.deleteById(id);
	}

}
